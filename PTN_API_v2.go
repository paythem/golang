package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/spacemonkeygo/openssl"
	"github.com/tkuchiki/go-timezone"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type Api struct {
	ENVIRONMENT           string
	SERVER_TRANSACTION_ID int
	RESULT_CODE           int
	HTTP_RESPONSE         string
	HTTP_CURL_ERROR       string
	IV                    string
	ERROR_DESCRIPTION     string
	RESULT                string
	RESPONSE              string
	CAPIV                 string
	CURLING               *http.Client
	HASH_MAC_ENC          string
	SERVER_URI            string
	ENCRYPTED_CONTENT     map[string]interface{}
	ENCRYPT_RESPONSE      bool
	DEBUG_OUTPUT          bool
	PUBLIC_KEY            string
	PRIVATE_KEY           string
	USERNAME              string
	PASSWORD              string
	FUNCTION              string
	SOURCE_IP             string
	PARAMETERS            map[string]interface{}
	FAULTY_PROXY          bool
	SERVER_TIMEZONE       string
}

func (api Api) dPrt(title string, variable interface{}, extraLine bool) {
	if api.DEBUG_OUTPUT {
		if reflect.TypeOf(variable).Kind() == reflect.Bool {
			variableBool, _ := strconv.ParseBool(fmt.Sprintf("%v", variable))
			println(fmt.Sprintf("%s", title) + ": " + strconv.FormatBool(variableBool))
			println()
			if extraLine {
				println()
			}
			return
		}
		if reflect.TypeOf(variable).Kind() == reflect.Slice || reflect.TypeOf(variable).Kind() == reflect.Map || reflect.TypeOf(variable).Kind() == reflect.Struct {
			b, err := json.MarshalIndent(variable, "", "  ")
			if err != nil {
				fmt.Println("error:", err)
			}
			println()
			println(fmt.Sprintf("%s", title) + ": ")
			println("-------------------------")
			print(string(b))
			println()
			println()
			if extraLine {
				println()
			}
			return
		}

		println(fmt.Sprintf("%s", title) + ": " + fmt.Sprintf("%s", variable))

		if extraLine {
			println()
		}

	}
}

func (api Api) checkVariable(variable, variableName, errCode, errMsg string) {
	api.dPrt("Checking", variableName, false)
	if variable == "" {
		println(fmt.Sprintf("ERROR %s: %s", errCode, errMsg))
		os.Exit(1)
	}
}

func (api Api) getServerIP(overrideIP string) string {
	if overrideIP == "" {
		externalIp, err := externalIP()
		if err != nil {
			println(err)
		}
		return externalIp
	} else {
		return overrideIP
	}
}

func externalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("error retrieving device ip address")
}

func (api Api) CallApi(debug bool) FinalServerAPIResponse {
	api.dPrt("Posting to (empty for production)", api.ENVIRONMENT, false)
	api.dPrt("Debug post (returns running commentary)", debug, false)

	// Set global debug flag.
	api.DEBUG_OUTPUT = debug

	// Fill in incomplete variables
	if api.SOURCE_IP == "" {
		api.SOURCE_IP = api.getServerIP("")
	}
	if api.PARAMETERS == nil {
		api.PARAMETERS = make(map[string]interface{})
	}

	// Check that all required variables are filled.
	api.checkVariable(api.PUBLIC_KEY, "PUBLIC_KEY", "API_C_00004", "Public key cannot be empty.")
	api.checkVariable(api.PRIVATE_KEY, "PRIVATE_KEY", "API_C_00005", "Private key cannot be empty.")
	api.checkVariable(api.USERNAME, "USERNAME", "API_C_00006", "Username cannot be empty.")
	api.checkVariable(api.PASSWORD, "PASSWORD", "API_C_00007", "Password cannot be empty.")
	api.checkVariable(api.FUNCTION, "FUNCTION", "API_C_00008", "No function specified.")

	// Build the content (POST) variable
	content := make(map[string]interface{})
	content["USERNAME"] = api.USERNAME
	content["ENCRYPT_RESPONSE"] = api.ENCRYPT_RESPONSE
	content["PASSWORD"] = api.PASSWORD
	content["FUNCTION"] = api.FUNCTION
	content["PARAMETERS"] = api.PARAMETERS
	content["API_VERSION"] = api.CAPIV
	content["SERVER_URI"] = api.SERVER_URI
	content["SERVER_TIMESTAMP"] = time.Now()
	content["SERVER_TIMEZONE"] = api.SERVER_TIMEZONE
	content["HASH_STUB"] = fmt.Sprintf("%010d", rand.Int63n(1e10))
	content["PUBLIC_KEY"] = api.PUBLIC_KEY

	jsonContent, err := json.Marshal(content)
	if err != nil {
		fmt.Println(err.Error())
	}
	stringJsonContent := string(jsonContent)

	api.dPrt("JSON string to post", content, false)

	// Generate the HMAC hash
	hash := genHMAC256(stringJsonContent, api.PRIVATE_KEY)

	api.dPrt("HMAC Hash of JSON string", hash, false)

	api.ENCRYPTED_CONTENT = make(map[string]interface{})
	api.ENCRYPTED_CONTENT["CONTENT"] = api.doEncrypt(stringJsonContent)

	encryptedContent := url.Values{}
	encryptedContent.Set("PUBLIC_KEY", api.PUBLIC_KEY)
	encryptedContent.Set("CONTENT", fmt.Sprintf("%v", api.ENCRYPTED_CONTENT["CONTENT"]))
	encryptedContent.Set("ZAPI", api.IV)
	api.dPrt("Encrypted POST content", encryptedContent, false)

	// Do cURL call
	req, err := http.NewRequest(http.MethodPost, api.SERVER_URI, strings.NewReader(encryptedContent.Encode()))
	if err != nil {
		log.Println(err)
	}

	// Set the headers for the POST
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("X-Public-Key", api.PUBLIC_KEY)
	req.Header.Set("X-Hash", hash)
	req.Header.Set("X-Sourceip", api.SOURCE_IP)
	if api.FAULTY_PROXY {
		req.Header.Set("X-Forwarded-For-Override", api.SOURCE_IP)
	}

	if api.DEBUG_OUTPUT {
		api.dPrt("Setting Header", fmt.Sprintf("Content-Type: application/x-www-form-urlencoded"), false)
		api.dPrt("Setting Header", fmt.Sprintf("X-Public-Key: %s", api.PUBLIC_KEY), false)
		api.dPrt("Setting Header", fmt.Sprintf("X-Hash: %s", hash), false)
		api.dPrt("Setting Header", fmt.Sprintf("X-Sourceip: %s", api.SOURCE_IP), true)
		if api.FAULTY_PROXY {
			api.dPrt("Setting Header", fmt.Sprintf("X-Forwarded-For-Override: %s", api.SOURCE_IP), true)
		}
	}

	res, err := api.CURLING.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		fmt.Println(err)

	}

	api.dPrt("API response: Original", string(body), false)
	// Process and decrypt the result
	var serverAPIResponse ServerAPIResponseRaw
	var finalServerAPIResponse FinalServerAPIResponse

	if api.ENCRYPT_RESPONSE {
		serverAPIResponse, err = UnmarshalServerAPIResponseRaw(body)
		if err != nil {
			log.Println(err)
		}
		decodedBytes, _ := base64.StdEncoding.DecodeString(serverAPIResponse.Content)
		decryptedBytes := api.doDecrypt(decodedBytes)
		var contentInterface interface{}
		err = json.Unmarshal(decryptedBytes, &contentInterface)
		if err != nil {
			log.Fatal(err)
		}
		finalServerAPIResponse = FinalServerAPIResponse{
			ServerTransactionID: serverAPIResponse.ServerTransactionID,
			Result:              serverAPIResponse.Result,
			ErrorDescription:    serverAPIResponse.ErrorDescription,
			Content:             contentInterface,
		}
	} else {
		serverAPIResponse, err = UnmarshalServerAPIResponseRaw(body)
		if err != nil {
			log.Println(err)
		}
		var contentInterface interface{}
		err = json.Unmarshal([]byte(serverAPIResponse.Content), &contentInterface)
		if err != nil {
			log.Fatal(err)
		}
		finalServerAPIResponse = FinalServerAPIResponse{
			ServerTransactionID: serverAPIResponse.ServerTransactionID,
			Result:              serverAPIResponse.Result,
			ErrorDescription:    serverAPIResponse.ErrorDescription,
			Content:             contentInterface,
		}
	}
	api.dPrt("API response: Decoded", finalServerAPIResponse, false)
	return finalServerAPIResponse
}

func getTimeZone() string {
	var returnTimeZone string
	timeZoneAbbr, timeZoneOffSet := time.Now().Zone()
	tz := timezone.New()
	tzInfo, _ := tz.GetTimezones(timeZoneAbbr)
	for _, tzI := range tzInfo {
		info, _ := tz.GetTzInfo(tzI)
		if timeZoneOffSet == info.StandardOffset() {
			returnTimeZone = tzI
			break
		}
	}
	if returnTimeZone == "" {
		log.Println("ERROR: Unable to get device timezone.")
		returnTimeZone = "UTC"
	}
	return returnTimeZone
}

func genHMAC256(contentString string, privateKey string) string {
	h := hmac.New(sha256.New, []byte(privateKey))
	h.Write([]byte(contentString))
	sha := hex.EncodeToString(h.Sum(nil))
	return sha
}

func (api Api) doDecrypt(content []byte) []byte {
	key := []byte(api.PRIVATE_KEY)
	iv := []byte(api.IV)
	crypter, _ := NewCrypt(key, iv)
	decoded, _ := crypter.Decrypt(content)
	return decoded
}

func (api Api) doEncrypt(content string) string {
	key := []byte(api.PRIVATE_KEY)
	iv := []byte(api.IV)
	crypter, _ := NewCrypt(key, iv)
	encoded, _ := crypter.Encrypt([]byte(content))
	return base64.StdEncoding.EncodeToString(encoded)
}

func PTN_API_v2(environment string, appID int, iv string) Api {
	var api Api
	api.CAPIV = "2.2.0"
	api.ENVIRONMENT = environment
	api.SERVER_URI = fmt.Sprintf("https://vvs%s.paythem.net/API/%d/", api.ENVIRONMENT, appID)
	api.IV = iv
	api.CURLING = &http.Client{}
	api.SERVER_TIMEZONE = getTimeZone()
	return api
}

type Crypt struct {
	key    []byte
	iv     []byte
	cipher *openssl.Cipher
}

func NewCrypt(key []byte, iv []byte) (*Crypt, error) {
	cipher, err := openssl.GetCipherByName("aes-256-cbc")
	if err != nil {
		return nil, err
	}

	return &Crypt{key, iv, cipher}, nil
}

func (c *Crypt) Encrypt(input []byte) ([]byte, error) {
	ctx, err := openssl.NewEncryptionCipherCtx(c.cipher, nil, c.key, c.iv)
	if err != nil {
		return nil, err
	}

	cipherBytes, err := ctx.EncryptUpdate(input)
	if err != nil {
		return nil, err
	}

	finalBytes, err := ctx.EncryptFinal()
	if err != nil {
		return nil, err
	}

	cipherBytes = append(cipherBytes, finalBytes...)
	return cipherBytes, nil
}

func (c *Crypt) Decrypt(input []byte) ([]byte, error) {
	ctx, err := openssl.NewDecryptionCipherCtx(c.cipher, nil, c.key, c.iv)
	if err != nil {
		return nil, err
	}

	cipherBytes, err := ctx.DecryptUpdate(input)
	if err != nil {
		return nil, err
	}

	finalBytes, err := ctx.DecryptFinal()
	if err != nil {
		return nil, err
	}

	cipherBytes = append(cipherBytes, finalBytes...)
	return cipherBytes, nil
}
func UnmarshalServerAPIResponseRaw(data []byte) (ServerAPIResponseRaw, error) {
	var r ServerAPIResponseRaw
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *ServerAPIResponseRaw) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type ServerAPIResponseRaw struct {
	ServerTransactionID string `json:"SERVER_TRANSACTION_ID,omitempty"`
	Result              string `json:"RESULT,omitempty"`
	ErrorDescription    string `json:"ERROR_DESCRIPTION,omitempty"`
	Content             string `json:"CONTENT,omitempty"`
}

func (r *FinalServerAPIResponse) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type FinalServerAPIResponse struct {
	ServerTransactionID string      `json:"SERVER_TRANSACTION_ID,omitempty"`
	Result              string      `json:"RESULT,omitempty"`
	ErrorDescription    string      `json:"ERROR_DESCRIPTION,omitempty"`
	Content             interface{} `json:"CONTENT,omitempty"`
}

//type Content struct {
//	OEMID           int64  `json:"OEM_ID,omitempty"`
//	OEMName         string `json:"OEM_Name,omitempty"`
//	OEMWebsite      string `json:"OEM_Website,omitempty"`
//	OEMProductCount int64  `json:"OEM_ProductCount,omitempty"`
//}
