package main

import (
	"fmt"
	"math/rand"
	"os"
)

func main() {
	api := PTN_API_v2("demo", 2824, fmt.Sprintf("%016d", rand.Int63n(1e16)))

	if api.ENVIRONMENT == "demo" {
		api.DEBUG_OUTPUT = false
		api.ENCRYPT_RESPONSE = false
		api.PUBLIC_KEY = ""
		api.PRIVATE_KEY = ""
		api.USERNAME = ""
		api.PASSWORD = ""
		api.FUNCTION = ""
	} else if api.ENVIRONMENT == "" { // Implied production
		api.DEBUG_OUTPUT = true
		api.ENCRYPT_RESPONSE = false
		api.PUBLIC_KEY = ""
		api.PRIVATE_KEY = ""
		api.USERNAME = ""
		api.PASSWORD = ""
		api.FUNCTION = ""
	} else {
		println("No ENVIRONMENT specified.")
		os.Exit(0)
	}

	//
	// setting an api function
	//

	// :: Calls without parameters
	//api.FUNCTION = "get_OEMList"
	//api.FUNCTION = "get_BrandList"
	api.FUNCTION = "get_ProductList"
	//api.FUNCTION = "get_AllProductAvailability"

	// :: Calls with parameters
	//api.FUNCTION = "get_SalesTransaction_ByDateRange" // Must be maximum 30 days apart.
	//parameters := make(map[string]interface{})
	//parameters["FROM_DATE"] = "2021-04-01 00:00:00"
	//parameters["TO_DATE"] =  "2021-04-21 23:59:59"
	//api.PARAMETERS = parameters

	//api.FUNCTION = "get_FinancialTransaction_ByDateRange" // Must be maximum 30 days apart.
	//parameters := make(map[string]interface{})
	//parameters["FROM_DATE"] = "2021-04-01 00:00:00"
	//parameters["TO_DATE"] =  "2021-04-21 23:59:59"
	//api.PARAMETERS = parameters

	//api.FUNCTION = "get_Vouchers"
	//parameters := make(map[string]interface{})
	//parameters["PRODUCT_ID"] = 1
	//parameters["QUANTITY"] = 1
	//parameters["REFERENCE_ID"] = "MY OWN REFERENCE ID"
	//api.PARAMETERS = parameters

	//api.Function = "get_ProductAvailability"
	//parameters := make(map[string]interface{})
	//parameters["PRODUCT_ID"] = 38
	//api.PARAMETERS = parameters

	//api.Function = "get_ErrorDescription"
	//parameters := make(map[string]interface{})
	//parameters["ERROR_ID"] = 38
	//api.PARAMETERS = parameters

	response := api.CallApi(api.DEBUG_OUTPUT)
	responseJson, _ := response.Marshal()
	println(string(responseJson))

}
